<?php

/**
 * @file
 * Contains \Drupal\paging\Form\PagingSettings.
 */

namespace Drupal\paging\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Link;
//use Drupal\node\Entity;

class PagingSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paging_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('paging.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['paging.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = [];

    // Set the id of the top-level form tag.
    $form['#id'] = 'paging';

    // General paging settings, not specific to node type.
    $form['paging_general'] = [
      '#type' => 'fieldset',
      '#title' => t('General paging settings'),
      '#collapsible' => FALSE,
    ];

    // Paging separator string.
    // @TODO will need an upgrade path.
    $form['paging_general']['paging_separator'] = [
      '#type' => 'textfield',
      '#title' => t('Page separator string'),
      '#size' => 20,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => \Drupal::config('paging.settings')->get('paging_separator','<!--pagebreak-->'),//variable_get('paging_separator', '<!--pagebreak-->'),
      '#description' => t('Use an HTML tag that will render reasonably when
      paging is not enabled, such as %pagebreak or %hr.', [
        '%pagebreak' => \Drupal::config('paging.settings')->get('paging_separator','<!--pagebreak-->'),
        '%hr' => '<hr />',
      ]),
    ];

    // Number of pagers on a page.
    // @TODO will need an upgrade path.
    $form['paging_general']['paging_pager_count'] = [
      '#type' => 'radios',
      '#title' => t('Number of Pagers on each page'),
      '#options' => [
        'one' => t('One'),
        'two' => t('Two'),
      ],
      '#required' => TRUE,
      '#description' => t('Choose how many pagers you would like on each page.
      The positioning of enabled pager(s) can be controlled for each
      <a href="@url">content type</a> under <em>Manage display</em>.', [
        '%none' => t('None'),
        '@paging' => '$node->paging',
        '@url' => \Drupal\Core\Url::fromUri('base://admin/structure/types'),//url('admin/structure/types'),
      ]),
      '#default_value' => \Drupal::config('paging.settings')->get('paging_pager_count','one'),// variable_get('paging_pager_count', 'one'),
      '#attributes' => [
        'class' => [
          'paging-pager'
          ]
        ],
    ];

    $paging_filter = FALSE;
    // Retrieve all input filters.
    foreach (filter_formats() as $format) {
      // Further retrieve all input formats.
      
      //Need to find a solution for filter_list_format
     /* foreach (filter_list_format($format->format) as $filter) {
        // Check if any of the input formats have paging filter enabled.
        if ($filter->module == 'paging') {
          $paging_filter = TRUE;
          break;
        }
      }
      */
      
    }
    if (!$paging_filter) {
      // Warn if paging filter is not yet enabled for any input format.
      drupal_set_message(t('Paging filter has not yet been enabled for any text
      formats. !link!', [
        '!link' => \Drupal::l(t('Enable now'), \Drupal\Core\Url::fromUri('base://admin/config/content/formats'))
        ]), 'warning paging-warning');
    }

    // Get all valid fields.
    $fields = \Drupal\field\Entity\FieldStorageConfig::loadByName();//field_info_fields();
    $field_options = [];
    // Remove fields that are not on nodes.
    $valid_bundles = ['node']; // TODO make this work for other entity types?
    // Remove fields that are not lontext, or longtext and sumamry from the list.
    $valid_fields = [
      'text_long',
      'text_with_summary',
    ];
    // Remove fields with multiple values.
    // @TODO
    foreach ($fields as $fieldname => $field) {
      $option = TRUE;
      if (!in_array($field['type'], $valid_fields)) {
        unset($fields[$fieldname]);
        $option = FALSE;
      }
      else {
        foreach ($valid_bundles as $bundle_name) {
          if (!array_key_exists($bundle_name, $field['bundles'])) {
            unset($fields[$fieldname]);
            $option = FALSE;
          }
        }
      }
      if ($option) {
        $field_options[$fieldname] = $fieldname;
      }
    }

    // Loop over all the available content types.
    foreach (node_type_get_types() as $type => $node_type) {
      $form[$type]['paging_config'] = [
        '#type' => 'fieldset',
        '#title' => ucfirst($type),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#attributes' => [
          'id' => 'paging-type-' . $type,
          'class' => [
            'paging-fieldset'
            ],
        ],
      ];

      // Left column fieldset.
      $form[$type]['paging_config']['paging_left'] = [
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#attributes' => [
          'class' => [
            'paging-left'
            ]
          ],
      ];

      // Paging toggle checkbox.
      $form[$type]['paging_config']['paging_left']['paging_enabled_' . $type] = [
        '#type' => 'checkbox',
        '#title' => 'Enable paging',
        '#default_value' => \Drupal::config('paging.settings')->get('paging_enabled_' . $type, 0),//variable_get('paging_enabled_' . $type, 0),
        '#attributes' => [
          'class' => [
            'paging-enabled'
            ]
          ],
      ];

      // Paging toggle checkbox.
      $form[$type]['paging_config']['paging_left']['paging_field_' . $type] = [
        '#type' => 'radios',
        '#title' => 'Select field to use for page breaks',
        '#options' => $field_options,
        '#default_value' => \Drupal::config('paging.settings')->get('paging_field_' . $type, 0),//variable_get('paging_field_' . $type, 0),
        '#attributes' => [
          'class' => [
            'paging-enabled'
            ]
          ],
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ]
            ]
          ],
      ];

      // Change "Read more" path when first page is greater than or equal to the teaser.
      $form[$type]['paging_config']['paging_left']['paging_read_more_enabled_' . $type] = [
        '#type' => 'checkbox',
        '#title' => t('Link "Read more" to second page'),
        '#description' => t('When enabled, the "Read more" link for teasers will
        link to the second page of the content if the teaser is larger than the
        first page or if they are the same.'),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_read_more_enabled_' . $type, 0),//variable_get('paging_read_more_enabled_' . $type, 0),
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ]
            ]
          ],
      ];

      // Set the browser's title to current page's name.
      $form[$type]['paging_config']['paging_left']['paging_name_title_' . $type] = [
        '#type' => 'checkbox',
        '#title' => t('Change page title to name of current page'),
        '#description' => t("Change the node's and browser window's title into
        name of the current page."),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_name_title_' . $type, 0),//variable_get('paging_name_title_' . $type, 0),
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ]
            ]
          ],
      ];

      // Right column fieldset.
      $form[$type]['paging_config']['paging_right'] = [
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#attributes' => [
          'class' => [
            'paging-right'
            ]
          ],
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ]
            ]
          ],
      ];

      // Optional automatic paging method. Each option opens the corresponding character/word length select list.
      // Accompanied by paging.admin.js.
      // @TODO this will need an upgrade path. (now specifying keys for options)
      $form[$type]['paging_config']['paging_right']['paging_automatic_method_' . $type] = [
        '#type' => 'radios',
        '#title' => t('Automatic paging method'),
        '#options' => [
          'disabled' => t('Disabled'),
          'chars' => t('Limit by characters <small>(recommended)</small>'),
          'words' => t('Limit by words'),
        ],
        '#required' => TRUE,
        '#description' => t('Method for automatic paging (ignored where paging
        separator string is used).'),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_automatic_method_' . $type, 'disabled'),//variable_get('paging_automatic_method_' . $type, 'disabled'),
        '#attributes' => [
          'class' => [
            'paging-method'
            ]
          ],
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ]
            ]
          ],
      ];

      // Get the length options.
      // @TODO: Do we really need 750?
      $char_len_options = [
        -1 => 750
        ] + range(500, 7500, 500);
      asort($char_len_options);
      $char_len_options = array_map($callback, array_combine($char_len_options, $char_len_options));//drupal_map_assoc($char_len_options);

      // Automatic paging method. Select list to choose the number of characters per page.
      $form[$type]['paging_config']['paging_right']['paging_automatic_chars_' . $type] = [
        '#type' => 'select',
        '#title' => t('Length of each page'),
        '#options' => $char_len_options,
        '#field_suffix' => t('characters'),
        '#required' => TRUE,
        '#description' => '<br />' . t('Number of characters to display per page.'),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_automatic_chars_' . $type, 4000),//variable_get('paging_automatic_chars_' . $type, 4000),
        '#prefix' => '<div class="container-inline paging-chars paging-chars-' . $type . '">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ],
            ':input[name="paging_automatic_method_' . $type . '"]' => [
              'value' => 'chars'
              ],
          ]
          ],
      ];

      // Automatic paging method. Text box to choose orphan size.
      $form[$type]['paging_config']['paging_right']['paging_automatic_chars_orphan_' . $type] = [
        '#type' => 'textfield',
        '#title' => t('Length of orphans'),
        '#size' => 6,
        '#field_suffix' => t('characters'),
        '#required' => TRUE,
        '#description' => '<br />' . t('Number of characters to consider as an orphan.'),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_automatic_chars_orphan_' . $type, 100),//variable_get('paging_automatic_chars_orphan_' . $type, 100),
        '#prefix' => '<div class="container-inline paging-chars-orphan paging-chars-orphan-' . $type . '">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ],
            ':input[name="paging_automatic_method_' . $type . '"]' => [
              'value' => 'chars'
              ],
          ]
          ],
      ];

      // Automatic paging method. Select list to choose the number of words per page.
      $array_config = range(100, 1000, 50);
      $form[$type]['paging_config']['paging_right']['paging_automatic_words_' . $type] = [
        '#type' => 'select',
        '#title' => t('Length of each page'),
        '#options' => array_map($callback, array_combine($array_config, $array_config)),//drupal_map_assoc(range(100, 1000, 50)),
        '#field_suffix' => t('words'),
        '#required' => TRUE,
        '#description' => '<br />' . t('Number of words to display per page.'),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_automatic_words_' . $type, 400),//variable_get('paging_automatic_words_' . $type, 400),
        '#prefix' => '<div class="container-inline paging-words paging-words-' . $type . '">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ],
            ':input[name="paging_automatic_method_' . $type . '"]' => [
              'value' => 'words'
              ],
          ]
          ],
      ];

      // Automatic paging method. Text box to set orphan page size.
      $form[$type]['paging_config']['paging_right']['paging_automatic_words_orphan_' . $type] = [
        '#type' => 'textfield',
        '#title' => t('Length of orphans'),
        '#size' => 6,
        '#field_suffix' => t('words'),
        '#required' => TRUE,
        '#description' => '<br />' . t('Number of wordss to consider as an orphan.'),
        '#default_value' => \Drupal::config('paging.settings')->get('paging_automatic_words_orphan_' . $type, 200),//variable_get('paging_automatic_words_orphan_' . $type, 200),
        '#prefix' => '<div class="container-inline paging-words-orphan paging-words-orphan-' . $type . '">',
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [// action to take.
            ':input[name="paging_enabled_' . $type . '"]' => [
              'checked' => TRUE
              ],
            ':input[name="paging_automatic_method_' . $type . '"]' => [
              'value' => 'words'
              ],
          ]
          ],
      ];
    }

    $module_path = drupal_get_path('module', 'paging');

    //drupal_add_css($module_path . '/paging.admin.css', 'module');
    $form['#attached']['library'][] = $module_path.'/paging.admin.css';

    return parent::buildForm($form, $form_state);
  }

}
